;;; transparency.el --- Transparency made easy!      -*- lexical-binding: t; -*-

;; Copyright (C) 2024  Alejandro Barocio A.

;; Author: Alejandro Barocio A. <alejandro@barocio.cc>
;; Version: 0.1.0
;; Package-Requires: ((emacs "29.05"))
;; Keywords: frames, convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; A minor-mode to enable and disable transparency mode.

;; This also makes possible to easyly configure transparency via
;; customization, and to change to a specific transparency with a
;; simple command.

;;; Code:

(if (version< emacs-version "29")
    (require 'transparency-legacy)

;;;; Variables:
  (defgroup transparency
    nil
    "Customize the values for transparency.")

  (defcustom transparency-default-alpha 75
    "The default to use with `transparency-reset'."
    :type '(choice (integer :tag "Percentage of opacity")
		   (const :tag "Off" nil))
    :group 'transparency)

  (defcustom transparency-alpha-default-previous 25
    "The default to use with `transparency-reset'."
    :type '(choice (integer :tag "Percentage of opacity")
		   (const :tag "Off" nil))
    :group 'transparency)

  (defcustom transparency-alpha nil
    "The active value for transparency."
    :type '(choice (integer :tag "Percentage of opacity")
		   (const :tag "Off" nil))
    :group 'transparency)

  (defcustom transparency-alpha-previous nil
    "The previous value for transparency."
    :type '(choice (integer :tag "Percentage of opacity")
		   (const :tag "Off" nil))
    :group 'transparency)

  (defcustom transparency-mode-key "C-c t"
    "The default user's binding to our keymap."
    :type 'string
    :require 'transparency
    :group 'transparency)

  (defvar-keymap transparency-mode-map
    :doc "Transparency default keymap."
    "C-c t RET" #'transparency-mode
    "C-c t t" #'transparency-toggle
    "C-c t c" #'transparency-change
    "C-c t r" #'transparency-reset)

;;;; Functions:
  (defun pgtk-p ()
    "Wether we are using PGTK (wayland?)."
    (when (member "PGTK"
                  (split-string
                   system-configuration-features
                   " "))
      t))

  ;; This is the function that actually changes the values of the
  ;; frame's alpha.
  (defun transparency-toggle--do-it (value &optional frame do-not-change-values)
    "Toggles the transparency."
    (let ((alpha (if (pgtk-p) 'alpha-background 'alpha)))
      (setopt default-frame-alist
              (remove (assq alpha default-frame-alist)
                      default-frame-alist))
      (if frame
          (set-frame-parameter frame alpha value)
        (dolist (fr (frame-list))
          (set-frame-parameter fr alpha value))
        (add-to-list 'default-frame-alist `(,alpha . ,value)))))

  ;; (defun transparency--init-map (&optional symbol value)
  ;;   "Initializes the transparency keymap."
  ;;   (when (and (boundp transparency-mode-key)
  ;;              transparency-mode-key)
  ;;     (keymap-set global-map transparency-mode-key transparency-mode-map)))
  (defun transparency--init-map (&optional symbol value)
    (ignore))

  (defun transparency--init ()
    "Initialize the transparency mode."
    (unless transparency-alpha
      (setopt transparency-alpha
              transparency-default-alpha))
    (unless transparency-alpha-previous
      (setopt transparency-alpha-previous
              transparency-alpha-default-previous))
    (transparency--init-map)
    (transparency-toggle--do-it transparency-alpha)
    (message "Transparency: %d%%" transparency-alpha))

  (defun transparency--end ()
    "Sets the alpha value of the frame(s) for toggling off the mode."
    (transparency-toggle--do-it 100))

;;;; Keymaps:

;;;; Minor-mode:
;;;###autoload
  (define-minor-mode transparency-mode
    "Activates transparency on your frames."
    :global t
    (if transparency-mode
        (transparency--init)
      (transparency--end)))

;;;; Commands:
  (defun transparency-reset ()
    "Resets the values of the current and previous alpha to the defaults."
    (interactive)
    (setopt transparency-alpha nil)
    (setopt transparency-alpha-previous nil)
    (when transparency-mode
      (transparency--init))
    (transparency-toggle--do-it transparency-alpha))

  (defun transparency-toggle (arg &optional frame)
    "Switches the alpha to the previous value.

If a numeric prefix is provided, that will become the new alpha."
    (interactive "P")
    (when transparency-mode
      (let ((new (or arg
                     transparency-alpha-previous)))
        (setopt transparency-alpha-previous
                transparency-alpha)
        (setopt transparency-alpha new)))
    (transparency-toggle--do-it transparency-alpha frame))

  (defun transparency-change (alpha-new &optional frame)
    "Change the value of alpha to ALPHA-NEW.

If no parameter is provided, it will prompt for a number."
    (interactive
     (list (read-number "Transparency's new alpha value: "
                        transparency-alpha)))
    (let ((new (or alpha-new
                   transparency-alpha-previous)))
      (setopt transparency-alpha-previous
              transparency-alpha)
      (setopt transparency-alpha new))
    (transparency-toggle--do-it transparency-alpha frame)))

(provide 'transparency)
;;; transparency.el ends here
